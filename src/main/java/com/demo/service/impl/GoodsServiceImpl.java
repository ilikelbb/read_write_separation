package com.demo.service.impl;

import com.demo.domain.Goods;
import com.demo.mapper.GoodsMapper;
import com.demo.service.GoodsService;
import com.demo.annotation.Master;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author wugang
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {
    /**
     * 保存
     * @param goods 商品
     * @return  是否成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveGoods(Goods goods){
       return this.save(goods);
    }

    /**
     * 删除
     *
     * @param id id
     * @return 是否成功
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteGoods(Long id) {
        return this.removeById(id);
    }

    /**
     * 查询全部
     *
     * @return 全部
     */
    @Override
    public List<Goods> getGoodsAll() {
        return this.list();
    }

    /**
     * 查询单个
     *
     * @param id id
     * @return 商品
     */
    @Master
    @Override
    public Goods getGoodsById(Long id) {
        return this.getById(id);
    }

}
