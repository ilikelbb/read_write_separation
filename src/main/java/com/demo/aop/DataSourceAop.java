package com.demo.aop;

import com.demo.config.DataSourceContextHolder;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 设置切面 执行具体方法选择的数据源
 * @author wugang
 */
@Aspect
@Component
public class DataSourceAop {

    /**
     * 需要读的方法,切面
     */
    @Pointcut("!@annotation(com.demo.annotation.Master)" +
            "&& (execution(* com.demo.service..*.select*(..)) " +
            "|| execution(* com.demo.service..*.get*(..)))" +
            "|| execution(* com.demo.service..*.query*(..)))")
    public void readPointcut() {

    }

    /**
     * 写切面
     */
    @Pointcut("@annotation(com.demo.annotation.Master) " +
            "|| execution(* com.demo.service..*.insert*(..))" +
            "|| execution(* com.demo.service..*.save*(..))" +
            "|| execution(* com.demo.service..*.add*(..))" +
            "|| execution(* com.demo.service..*.update*(..))" +
            "|| execution(* com.demo.service..*.edit*(..))" +
            "|| execution(* com.demo.service..*.delete*(..))" +
            "|| execution(* com.demo.service..*.remove*(..))")
    public void writePointcut() {

    }

    @Before("readPointcut()")
    public void read() {
        DataSourceContextHolder.slave();
    }

    @Before("writePointcut()")
    public void write() {
        DataSourceContextHolder.master();
    }

    @After("readPointcut()")
    public void readAfter() {
        DataSourceContextHolder.clear();
    }

    @After("writePointcut()")
    public void writeAfter() {
        DataSourceContextHolder.clear();
    }
}
