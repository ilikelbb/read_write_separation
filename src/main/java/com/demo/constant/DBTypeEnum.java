package com.demo.constant;

/**
 * @author wugang
 * @desc 数据库类型枚举
 */

public enum DBTypeEnum {
    /**
     * 主节点
     */
    MASTER,
    /**
     * 从1
     */
    SLAVE1,
    /**
     * 从2
     */
    SLAVE2;

}