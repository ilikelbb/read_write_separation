package com.demo.controller;

import com.demo.domain.Goods;
import com.demo.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author wugang
 */
@RestController
@RequestMapping("goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 保存
     * @param goods 商品
     * @return  是否成功
     */
    @PostMapping("/saveGoods")
    public boolean saveGoods(@RequestBody Goods goods){
        return goodsService.saveGoods(goods);
    }

    /**
     * 删除
     *
     * @param id id
     * @return 是否成功
     */
    @DeleteMapping("/deleteGoods")
    public boolean deleteGoods(@RequestParam Long id) {
        return goodsService.deleteGoods(id);
    }

    /**
     * 查询全部
     *
     * @return 全部
     */
    @GetMapping("/getGoodsAll")
    public List<Goods> getGoodsAll() {
        return goodsService.getGoodsAll();
    }

    /**
     * 查询单个
     *
     * @param id id
     * @return 商品
     */
    @GetMapping("getGoodsById")
    public Goods getGoodsById(@RequestParam Long id) {
        return goodsService.getGoodsById(id);
    }
}
