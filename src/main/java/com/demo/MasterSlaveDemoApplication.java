package com.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wugang
 */
@SpringBootApplication
@MapperScan("com.demo.mapper")
public class MasterSlaveDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MasterSlaveDemoApplication.class, args);
    }

}
